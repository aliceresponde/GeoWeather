package com.aliceresponde.geoweather.Adapters;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.aliceresponde.geoweather.DTO.Forecast;
import com.aliceresponde.geoweather.R;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.List;

/**
 * Created by alice on 4/16/16.
 */
public class MyWeatherAdapter extends RecyclerView.Adapter<MyWeatherAdapter.ViewHolder> {
  Context context;
  List<Forecast> mForecast;
  List<MarkerOptions> mMarkers;
  private static LayoutInflater inflater = null;

  public MyWeatherAdapter(Context context, List<Forecast> mForecast, List<MarkerOptions> mMarkers) {
    this.context = context;
    this.mForecast = mForecast;
    this.mMarkers = mMarkers;
    inflater = LayoutInflater.from(context);
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = inflater.inflate(R.layout.weather_item_row, parent, false);
    ViewHolder holder = new ViewHolder(view);
    return holder;
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    if (position < 2) {
      holder.tvCity.setText(mMarkers.get(position).getTitle());
      holder.tvMain.setText(mForecast.get(position).getWeather().get(0).getMain());
      setImageV(holder);
    }
  }

  private void setImageV(ViewHolder holder) {
    if (holder.tvMain.getText()
        .toString()
        .equalsIgnoreCase(context.getString(R.string.weather_rain))) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        holder.imageView.setImageDrawable(context.getDrawable(R.drawable.rain));
      }
    } else if (holder.tvMain.getText()
        .toString()
        .equalsIgnoreCase(context.getString(R.string.weather_clear))) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        holder.imageView.setImageDrawable(context.getDrawable(R.drawable.clear));
      }
    } else if (holder.tvMain.getText()
        .toString()
        .equalsIgnoreCase(context.getString(R.string.weather_clouds))) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        holder.imageView.setImageDrawable(context.getDrawable(R.drawable.clouds));
      }
    }
  }

  @Override public long getItemId(int position) {
    return 0;
  }

  @Override public int getItemCount() {
    return mForecast.size();
  }

  static class ViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.tv_main) TextView tvMain;
    @Bind(R.id.tv_city) TextView tvCity;
    @Bind(R.id.imageView) ImageView imageView;

    ViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }
}
