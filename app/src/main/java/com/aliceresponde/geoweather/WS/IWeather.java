package com.aliceresponde.geoweather.WS;

import android.support.v4.app.Fragment;

import com.aliceresponde.geoweather.DTO.DroneResponse;
import com.aliceresponde.geoweather.DTO.Forecast;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by alice on 4/15/16.
 * http://api.openweathermap.org/data/2.5/forecast?lat=35&lon=139
 */
public interface IWeather {

  //api.openweathermap.org/data/2.5/weather?lat=35&lon=139&units=metric
  @GET("/data/2.5/weather") Call<Forecast> getForecast(
      @Query("lat") double lat,
      @Query("lon") double lon,
      @Query("units") String units,
      @Query("APPID") String api_key);


  @GET("/inputs/0") Call<DroneResponse> getDroneData();
}
