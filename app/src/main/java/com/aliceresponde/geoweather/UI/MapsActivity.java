package com.aliceresponde.geoweather.UI;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aliceresponde.geoweather.Adapters.MyWeatherAdapter;
import com.aliceresponde.geoweather.BaseApplication;
import com.aliceresponde.geoweather.DTO.DroneResponse;
import com.aliceresponde.geoweather.DTO.Forecast;
import com.aliceresponde.geoweather.R;
import com.aliceresponde.geoweather.WS.IWeather;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MapsActivity extends FragmentActivity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = MapsActivity.class.getSimpleName();

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static int UPDATE_INTERVAL = 10000;
    private static int FATEST_INTERVAL = 5000;
    private static int DISPLACEMENT = 10;
    @Bind(R.id.root)
    View root;

    @Bind(R.id.tv_weather_city)
    TextView tvWeatherCity;
    @Bind(R.id.tv_weather_temp)
    TextView tvWeatherTemp;
    @Bind(R.id.tv_weather_min_temp)
    TextView tvWeatherMinTemp;
    @Bind(R.id.tv_weather_max_temp)
    TextView tvWeatherMaxTemp;
    @Bind(R.id.tv_weather_main)
    TextView tvWeatherMain;
    @Bind(R.id.tv_weather_description)
    TextView tvWeatherDescription;
    @Bind(R.id.tv_weather_wind_speed)
    TextView tvWeatherWindSpeed;
    @Bind(R.id.tv_weather_wind_direction)
    TextView tvWeatherWindDirection;

    @Bind(R.id.tv_drone_temp)
    TextView tvDroneTemp;
    @Bind(R.id.tv_drone_co2)
    TextView tvDroneCo2;
    @Bind(R.id.tv_drone_humidity)
    TextView tvDroneHumidity;
    @Bind(R.id.tv_drone_height)
    TextView tvDroneHeight;
    @Bind(R.id.tv_drone_x)
    TextView tvDroneX;
    @Bind(R.id.tv_drone_y)
    TextView tvDroneY;
    @Bind(R.id.tv_drone_z)
    TextView tvDroneZ;

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    private Location mLastLocation;
    private boolean mRequesLocationUpdates = false;
    MyWeatherAdapter adapter;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private List<MarkerOptions> myMarkers = new ArrayList<>();
    private List<Forecast> mForecast = new ArrayList<>();
    private LinearLayoutManager managerList;
    private Location location;



    private GoogleMap.OnMapClickListener onMapClick = new GoogleMap.OnMapClickListener() {
        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onMapClick(LatLng place) {

//      if (myMarkers.size() < 2) {
//        String cityNamme = getCityNameBy(place);
//        MarkerOptions markerO = new MarkerOptions().position(place).title(cityNamme);
//        myMarkers.add(markerO);
//        mMap.addMarker(markerO);
//      } else {
//        Snackbar.make(root, "You  have two places, please clear the map", Snackbar.LENGTH_LONG)
//            .setAction("Clear", new View.OnClickListener() {
//              @Override public void onClick(View v) {
//                clearMap();
//              }
//            })
//            .setActionTextColor(Color.YELLOW)
//            .show();
//      }
        }
    };
    private Marker droneMarker;


    /**
     * use  geoLocation  to  know  city name
     */
    private String getCityNameBy(LatLng place) {
        try {

            Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(place.latitude, place.longitude, 1);
            if (addresses.isEmpty()) {
                Log.d("address ", "N/A");
            } else {
                if (addresses.size() > 0) {
                    return addresses.get(0).getAddressLine(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }

        return "";
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Maps", "onCreate");

        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnMapClickListener(onMapClick);


        droneMarker = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_drone_16))
                .position(new LatLng(0, 0))
                .draggable(false));

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        if (checkPlayServices()) {
            buildGoogleApiClient();
            createLocationRequest();
        }

        drawRestictedAreas();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Log.d("Maps", "onStart");
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.aliceresponde.geoweather/http/host/path"));
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Maps", "onResume");
        checkPlayServices();
        if (mGoogleApiClient.isConnected() && mRequesLocationUpdates) {
            startLocationUpdates();
        }

        synchronized (this) {
            centerMapOnMyLocation();
        }


        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                if (location != null) {
                    if (isOnline()) {
                        synchronized (this) {
                            retreiveDroneData();
                        }
                    }else{
                        showAlertMessage("Not internet conection in your devise");
                    }
                }
            }
        }, 0, 5000);

        Timer timerW = new Timer();
        timerW.schedule(new TimerTask() {
            @Override
            public void run() {
                if (location != null) {
                    if (isOnline()) {

                        synchronized (this) {
                            retreiveWeather();
                        }

                    }else{
                        showAlertMessage("Not internet conection in your devise");
                    }
                }
            }
        }, 0, 50000);


    }


    protected void startLocationUpdates() {
        Log.d("Maps", "startLocationUpdates");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            Log.d("Maps mLastLocation", mLastLocation.toString());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.aliceresponde.geoweather/http/host/path"));
        AppIndex.AppIndexApi.end(client, viewAction);
        Log.d("Maps", "onStop");

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.disconnect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Maps", "onPause");
//        stopLocationUpdates();
//        mGoogleApiClient.disconnect();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(), "This device is not supperted", Toast.LENGTH_SHORT)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    private void centerMapOnMyLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        location = locationManager.getLastKnownLocation(locationManager
                .getBestProvider(criteria, false));

        if (location != null) {

            Log.i("CENTER", "-------------------------------");
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(location.getLatitude(), location.getLongitude()), 18));

            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(18)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private void drawRestictedAreas() {
        Polygon Facultad_de_Derecho = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.626734, -74.0648601),
                        new LatLng(4.626700, -74.0647133),
                        new LatLng(4.626830, -74.0646771),
                        new LatLng(4.626874, -74.0648232),
                        new LatLng(4.626734, -74.0648601))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150))
        );

        Polygon Aeropuerto_Internacional_El_Dorado = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6954676, -74.1518354),
                        new LatLng(4.6833632, -74.1355705),
                        new LatLng(4.6841330, -74.1351843),
                        new LatLng(4.6909766, -74.1303778),
                        new LatLng(4.6866566, -74.1249275),
                        new LatLng(4.6872127, -74.1242838),
                        new LatLng(4.6876404, -74.1231251),
                        new LatLng(4.6881109, -74.1228676),
                        new LatLng(4.6899073, -74.1247559),
                        new LatLng(4.6907199, -74.1238546),
                        new LatLng(4.6899927, -74.1228247),
                        new LatLng(4.6913615, -74.1218805),
                        new LatLng(4.6922168, -74.1230392),
                        new LatLng(4.6931579, -74.1225243),
                        new LatLng(4.7077856, -74.1419649),
                        new LatLng(4.6954676, -74.1518354))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon monumento_Banderas = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6314627, -74.147895),
                        new LatLng(4.6316365, -74.148064),
                        new LatLng(4.6317006, -74.148268),
                        new LatLng(4.6316391, -74.148517),
                        new LatLng(4.6314948, -74.148657),
                        new LatLng(4.6312969, -74.148708),
                        new LatLng(4.6311179, -74.148681),
                        new LatLng(4.6309468, -74.148541),
                        new LatLng(4.6308585, -74.148380),
                        new LatLng(4.6308611, -74.148179),
                        new LatLng(4.6309601, -74.148016),
                        new LatLng(4.6310803, -74.147914),
                        new LatLng(4.6312006, -74.147849),
                        new LatLng(4.6312676, -74.147731),
                        new LatLng(4.6313932, -74.147366),
                        new LatLng(4.6314601, -74.147366),
                        new LatLng(4.6314627, -74.147895))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon aeropuerto_Guaymaral = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.818509, -74.0742445),
                        new LatLng(4.814318, -74.0744591),
                        new LatLng(4.809572, -74.0575075),
                        new LatLng(4.809016, -74.0576792),
                        new LatLng(4.808117, -74.0547609),
                        new LatLng(4.811753, -74.0536022))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon aviacion_Armada_Nacional = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.707785, -74.141964),
                        new LatLng(4.717451, -74.154195),
                        new LatLng(4.717366, -74.155097),
                        new LatLng(4.717622, -74.156599),
                        new LatLng(4.720189, -74.159903),
                        new LatLng(4.715099, -74.169173),
                        new LatLng(4.711078, -74.172306),
                        new LatLng(4.710522, -74.171748),
                        new LatLng(4.708897, -74.170332),
                        new LatLng(4.705091, -74.170632),
                        new LatLng(4.704363, -74.167628),
                        new LatLng(4.705604, -74.165053),
                        new LatLng(4.695467, -74.151835),
                        new LatLng(4.707785, -74.141964))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon distrito_Militar_Nº3 = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6275595, -74.149920),
                        new LatLng(4.6273991, -74.149748),
                        new LatLng(4.6275915, -74.149518),
                        new LatLng(4.6275541, -74.149346),
                        new LatLng(4.6272975, -74.149024),
                        new LatLng(4.6288320, -74.147490),
                        new LatLng(4.6294897, -74.148144),
                        new LatLng(4.6275595, -74.149920))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon escuela_Superior_de_Guerra = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6840581, -74.040695),
                        new LatLng(4.6846677, -74.042009),
                        new LatLng(4.6839192, -74.042353),
                        new LatLng(4.6833685, -74.041076),
                        new LatLng(4.6840581, -74.040695))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon escuela_Militar_José_María_Córdova = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.679150, -74.073622),
                        new LatLng(4.678209, -74.072270),
                        new LatLng(4.677695, -74.072613),
                        new LatLng(4.674979, -74.068429),
                        new LatLng(4.678765, -74.065961),
                        new LatLng(4.682785, -74.070704),
                        new LatLng(4.683021, -74.071304),
                        new LatLng(4.682443, -74.072034),
                        new LatLng(4.679150, -74.073622))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon escuela_de_Ingenieros_Militar = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6337671, -74.101924),
                        new LatLng(4.6288802, -74.105690),
                        new LatLng(4.6285272, -74.105916),
                        new LatLng(4.6281209, -74.105840),
                        new LatLng(4.6277787, -74.105540),
                        new LatLng(4.6271905, -74.100905),
                        new LatLng(4.6276504, -74.100841),
                        new LatLng(4.6286342, -74.101978),
                        new LatLng(4.6287732, -74.101946),
                        new LatLng(4.6315001, -74.099779),
                        new LatLng(4.6337671, -74.101924))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon escuela_de_Caballeria = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6801338, -74.0377235),
                        new LatLng(4.6787652, -74.0357494),
                        new LatLng(4.6789363, -74.0333462),
                        new LatLng(4.6819303, -74.0319943),
                        new LatLng(4.6832776, -74.0336037),
                        new LatLng(4.6814384, -74.0362644),
                        new LatLng(4.6815454, -74.0365648),
                        new LatLng(4.6814384, -74.0367365),
                        new LatLng(4.6815454, -74.0370369),
                        new LatLng(4.6801338, -74.0377235))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon grupo_de_Acciones_Psicosociales_de_la_Policia_Nacional = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.675840, -74.090665),
                        new LatLng(4.676129, -74.090439),
                        new LatLng(4.676369, -74.090783),
                        new LatLng(4.676081, -74.091002),
                        new LatLng(4.675840, -74.090665))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon direccion_General_De_La_Policía_Nacional = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6476396, -74.0984219),
                        new LatLng(4.6473482, -74.0982127),
                        new LatLng(4.6467787, -74.0978131),
                        new LatLng(4.6472466, -74.0970513),
                        new LatLng(4.6475407, -74.0972632),
                        new LatLng(4.6476983, -74.0969977),
                        new LatLng(4.6483239, -74.0974188),
                        new LatLng(4.6476396, -74.0984219))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon centro_Medico_de_Policía_Chapinero = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6546250, -74.0629578),
                        new LatLng(4.6540263, -74.0633172),
                        new LatLng(4.6537535, -74.0628988),
                        new LatLng(4.6543899, -74.0628237),
                        new LatLng(4.6546250, -74.0629578))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon escuela_de_Postgrados_de_Policía = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.7364252, -74.071776),
                        new LatLng(4.7352224, -74.072077),
                        new LatLng(4.7351636, -74.071835),
                        new LatLng(4.7351155, -74.071825),
                        new LatLng(4.7353133, -74.071632),
                        new LatLng(4.7355324, -74.071304),
                        new LatLng(4.7360991, -74.070854),
                        new LatLng(4.7364252, -74.071776))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon polígono_18 = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6479845, -74.0783215),
                        new LatLng(4.6474391, -74.0783858),
                        new LatLng(4.6473856, -74.0778226),
                        new LatLng(4.6479417, -74.0777797),
                        new LatLng(4.6479845, -74.0783215))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon policia_Nacional = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.8174084, -74.0710044),
                        new LatLng(4.8165105, -74.0712297),
                        new LatLng(4.8162539, -74.0701568),
                        new LatLng(4.8170771, -74.0699959),
                        new LatLng(4.8174084, -74.0710044))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon comando_De_Departamento_De_Policía_Cundinamarca = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6291515, -74.1130333),
                        new LatLng(4.6293454, -74.1128442),
                        new LatLng(4.6294991, -74.1130158),
                        new LatLng(4.6293079, -74.1131875),
                        new LatLng(4.6291515, -74.1130333))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon ministerio_de_Agricultura_y_Desarrollo_Rural = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6018990, -74.074287),
                        new LatLng(4.6014177, -74.074587),
                        new LatLng(4.6012147, -74.074223),
                        new LatLng(4.6016959, -74.073965),
                        new LatLng(4.6018990, -74.074287))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon ministerio_de_Ambiente_y_Desarrollo_Sostenible = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.625019, -74.0667558),
                        new LatLng(4.625795, -74.0665895),
                        new LatLng(4.625918, -74.0674263),
                        new LatLng(4.625158, -74.0675497),
                        new LatLng(4.625019, -74.0667558))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon ministerio_de_Comercio_Industria_y_Turismo = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6157534, -74.070999),
                        new LatLng(4.6157801, -74.071044),
                        new LatLng(4.6158443, -74.071044),
                        new LatLng(4.6158682, -74.071079),
                        new LatLng(4.6160287, -74.071004),
                        new LatLng(4.6161276, -74.071216),
                        new LatLng(4.6159726, -74.071294),
                        new LatLng(4.6160341, -74.071444),
                        new LatLng(4.6156945, -74.071632),
                        new LatLng(4.6154539, -74.071149),
                        new LatLng(4.6157534, -74.070999))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon ministerio_de_Defensa_Nacional = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6459259, -74.095037),
                        new LatLng(4.6449742, -74.096549),
                        new LatLng(4.6434450, -74.095594),
                        new LatLng(4.6444395, -74.094125),
                        new LatLng(4.6459259, -74.095037))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));
        Polygon ministerio_de_Educación_Nacional = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.6469738, -74.0949833),
                        new LatLng(4.6464713, -74.0947258),
                        new LatLng(4.6463109, -74.0947044),
                        new LatLng(4.6461719, -74.0945971),
                        new LatLng(4.6463644, -74.0942645),
                        new LatLng(4.6471128, -74.0947473),
                        new LatLng(4.6469738, -74.0949833))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));

        Polygon alcaldía_de_Bogotá = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(4.5987229, -74.0761113),
                        new LatLng(4.5978673, -74.0766585),
                        new LatLng(4.5973968, -74.0758967),
                        new LatLng(4.5982737, -74.0754354),
                        new LatLng(4.5987229, -74.0761113))
                .strokeColor(Color.RED)
                .strokeWidth(4f)
                .fillColor(Color.argb(80, 30, 73, 150)));


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mRequesLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        this.location = location;
        Log.i("Maps", "Location Changed" + mLastLocation.toString());
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("Maps", "Connection Fail" + connectionResult.getErrorCode());
    }


    private void retreiveDroneData() {
        getServiceDrone().getDroneData().enqueue(new Callback<DroneResponse>() {
            @Override
            public void onResponse(Response<DroneResponse> response, Retrofit retrofit) {

                DroneResponse droneDataObject = response.body();
                Log.i("WS -Drone", droneDataObject.getLatitud() + "," + droneDataObject.getLongitud());

                tvDroneTemp.setText(droneDataObject.getTemperatura());
                tvDroneCo2.setText(droneDataObject.getCO());
                tvDroneHumidity.setText(droneDataObject.getHumedad());
                tvDroneHeight.setText(droneDataObject.getAltura() + "");
                tvDroneX.setText(droneDataObject.getX());
                tvDroneY.setText(droneDataObject.getY());
                tvDroneZ.setText(droneDataObject.getZ());

                droneMarker.remove();
                LatLng myLocation;
                try {
//                  LatLng myLocation = new LatLng(0,0);
                    myLocation = new LatLng(Double.parseDouble(droneDataObject.getLatitud()),
                            Double.parseDouble(droneDataObject.getLongitud()));
                    droneMarker = mMap.addMarker(new MarkerOptions()
                            .snippet("Drone")
                            .position(myLocation)
                            .draggable(false));
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                }
                if (!(Double.parseDouble(droneDataObject.getHumedad()) >= 10 && Double.parseDouble(droneDataObject.getHumedad()) <= 95)) {
                    showAlertMessage("Your humidity is out of acurate flight range");
                }
                else if ( Double.parseDouble(droneDataObject.getCO()) >= 180 ){
                    showAlertMessage("Too much pollution, low  visibility");
                }
//                try {
//
//                    Log.i("Altitud Driver",mLastLocation.getAltitude() +"");
//                    Log.i("Altitud Drone", droneDataObject.getAltura() +"");
//                    Log.i("Altitud Delta", Math.abs(mLastLocation.getAltitude() - droneDataObject.getAltura()) +"");
//
//                    if (Math.abs(mLastLocation.getAltitude() - droneDataObject.getAltura()) > 275) {
//                        showAlertMessage("The drone is to higth");
//                    }
//                }catch (Exception e ){
//                    Log.e("Error", e.getMessage());
//                }
//
////                showAlertMessage("You are to close to restricted area");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Error Drone", t.getMessage());


            }
        });

    }

    private void retreiveWeather() {
        IWeather service = getRetrofit().create(IWeather.class);

        Call<Forecast> responseF =
                service.getForecast(location.getLatitude(), location.getLongitude(), "metric",
                        "688b0459a554cb6a393d20e9acba8047");
        responseF.enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(Response<Forecast> response, Retrofit retrofit) {
                Forecast forecastObject = response.body();
                Log.i("WS - response", forecastObject.getCoord().getLat() + "," + forecastObject.getCoord().getLon());
                tvWeatherCity.setText(forecastObject.getName());

                tvWeatherTemp.setText(forecastObject.getMain().getTemp() + "");
                tvWeatherMinTemp.setText(forecastObject.getMain().getTempMin() + "");
                tvWeatherMaxTemp.setText(forecastObject.getMain().getTempMax() + "");

                if (forecastObject.getWeather().get(0).getMain().equalsIgnoreCase("Rain")) {
                    tvWeatherMain.setText(forecastObject.getWeather().get(0).getMain());
                    tvWeatherMain.setTextColor(Color.RED);
                    showAlertMessage("Is raining , Bad  weather to fly");

                }else{
                    tvWeatherMain.setText(forecastObject.getWeather().get(0).getMain());
                }

                tvWeatherDescription.setText(forecastObject.getWeather().get(0).getDescription());

                tvWeatherWindSpeed.setText(forecastObject.getWind().getSpeed() + "");
                tvWeatherWindDirection.setText(forecastObject.getWind().getDeg() + "");

                if (forecastObject.getWind().getSpeed()*3.6 > 30){
                    showAlertMessage("Wind speed over  30km/h");
                }
//        mForecast.add(response.body());
//        adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("errorWW", t.toString());
            }
        });
    }

    public void showAlertMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();

    }


    public Retrofit getRetrofit() {
        return ((BaseApplication) getApplicationContext()).getRetrofitWeather();
    }

    public IWeather getServiceDrone() {
        return ((BaseApplication) getApplicationContext()).getServiceDrone();
    }



    private boolean isGPSActive() {
        return ((BaseApplication) getApplicationContext()).isGPSActive();
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
}
