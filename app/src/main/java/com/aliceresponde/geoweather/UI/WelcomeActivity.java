package com.aliceresponde.geoweather.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.aliceresponde.geoweather.BaseApplication;
import com.aliceresponde.geoweather.R;

public class WelcomeActivity extends AppCompatActivity {

  @Bind(R.id.imageV_clear) ImageView imageVSummer;
  @Bind(R.id.imageV_cloudy) ImageView imageVCloudy;
  @Bind(R.id.imageV_rain) ImageView imageVRain;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_welcome);
    ButterKnife.bind(this);
  }

  public void goToMap(String weather) {
    Intent intent = new Intent(this, MapsActivity.class);
    intent.putExtra("weather", weather);
    startActivity(intent);
  }

  @OnClick({ R.id.imageV_clear, R.id.imageV_cloudy, R.id.imageV_rain })
  public void onClick(View view) {
    Log.d("GPS?" , isGPSActive()+" ");
    Log.d("internet?" , isOnline()+"  " );

    if (!isOnline()) {
      AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
      alertDialog.setTitle("Alert");
      alertDialog.setMessage("No internet Connection").show();
    } else if (!isGPSActive()) {
      AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
      alertDialog.setTitle("Alert");
      alertDialog.setMessage("GPS is not able").show();
    } else {
      switch (view.getId()) {
        case R.id.imageV_clear:
          goToMap("clear");
          break;
        case R.id.imageV_cloudy:
          goToMap("cloudy");
          break;
        case R.id.imageV_rain:
          goToMap("rain");
          break;
      }
    }
  }

  private boolean isOnline() {
    return ((BaseApplication) getApplicationContext()).isOnline();
  }

  private boolean isGPSActive() {
    return ((BaseApplication) getApplicationContext()).isGPSActive();
  }
}
