package com.aliceresponde.geoweather.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by alice on 4/18/16.
 */
public class Rain {

  @SerializedName("3h")
  @Expose
  private float _3h;

  public float get_3h() {
    return _3h;
  }

  public void set_3h(float _3h) {
    this._3h = _3h;
  }
}
