package com.aliceresponde.geoweather.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by alice on 4/18/16.
 */
public class Clouds {
  @SerializedName("all")
  @Expose
  private int all;

  public int getAll() {
    return all;
  }

  public void setAll(int all) {
    this.all = all;
  }
}
