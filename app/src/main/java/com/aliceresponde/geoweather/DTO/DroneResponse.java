package com.aliceresponde.geoweather.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by alice on 4/23/16.
 */
public class DroneResponse {

    @SerializedName("temperatura")
    @Expose
    private String temperatura;
    @SerializedName("humedad")
    @Expose
    private String humedad;
    @SerializedName("altura")
    @Expose
    private double altura;
    @SerializedName("CO")
    @Expose
    private String CO;
    @SerializedName("X")
    @Expose
    private String X;
    @SerializedName("Y")
    @Expose
    private String Y;
    @SerializedName("Z")
    @Expose
    private String Z;
    @SerializedName("Latitud")
    @Expose
    private String Latitud;
    @SerializedName("Longitud")
    @Expose
    private String Longitud;

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public String getHumedad() {
        return humedad;
    }

    public void setHumedad(String humedad) {
        this.humedad = humedad;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public String getCO() {
        return CO;
    }

    public void setCO(String CO) {
        this.CO = CO;
    }

    public String getX() {
        return X;
    }

    public void setX(String x) {
        X = x;
    }

    public String getY() {
        return Y;
    }

    public void setY(String y) {
        Y = y;
    }

    public String getZ() {
        return Z;
    }

    public void setZ(String z) {
        Z = z;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLatitud(String latitud) {
        Latitud = latitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLongitud(String longitud) {
        Longitud = longitud;
    }
}
