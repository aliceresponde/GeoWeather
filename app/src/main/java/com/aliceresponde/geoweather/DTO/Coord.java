package com.aliceresponde.geoweather.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by alice on 4/18/16.
 */
public class Coord {
  @SerializedName("lon")
  @Expose
  private float lon;
  @SerializedName("lat")
  @Expose
  private float lat;

  public float getLon() {
    return lon;
  }

  public void setLon(float lon) {
    this.lon = lon;
  }

  public float getLat() {
    return lat;
  }

  public void setLat(float lat) {
    this.lat = lat;
  }
}
