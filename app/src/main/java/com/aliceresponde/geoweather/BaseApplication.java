package com.aliceresponde.geoweather;

import android.app.Application;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.aliceresponde.geoweather.WS.IWeather;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by alice on 4/15/16.
 */
public class BaseApplication extends Application {
  private ConnectivityManager connectivityManager;
  private NetworkInfo networkInfo;
  private Retrofit retrofitWeather;
  private Retrofit retrofitDrone;
  private LocationManager manager;
  private IWeather serviceWeather;
  private IWeather serviceDrone;

  @Override public void onCreate() {
    super.onCreate();

    //Rest Adapter W
    retrofitWeather = new Retrofit.Builder()
        .baseUrl(getString(R.string.weather_api_base_url))
        .addConverterFactory(GsonConverterFactory.create())
        .build();

     serviceWeather = retrofitWeather.create(IWeather.class);


    retrofitDrone = new Retrofit.Builder()
            .baseUrl(getString(R.string.drone_api_base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    serviceDrone = retrofitDrone.create(IWeather.class);

    //Internet
    connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
    networkInfo = connectivityManager.getActiveNetworkInfo();

    manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );


  }

  public Retrofit getRetrofitWeather() {
    return retrofitWeather;
  }

  public IWeather getServiceWeather() {
    return serviceWeather;
  }

  public IWeather getServiceDrone() {
    return serviceDrone;
  }

  public Retrofit getRetrofitDrone() {
    return retrofitDrone;
  }

  public boolean isOnline() {
    return networkInfo != null && networkInfo.isConnected();
  }

  public boolean isGPSActive(){
     return manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ;
  }
}
